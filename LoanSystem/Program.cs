﻿using LoanSystem;
using LoanSystem.UseCases;
using LoanSystem.UseCases.Books;
using LoanSystem.UseCases.Customer;
using LoanSystem.UseCases.Loan;
using Microsoft.EntityFrameworkCore;

var contextOptions = new DbContextOptionsBuilder<CustomDbContext>()
    .UseNpgsql("User ID=postuser;Password=Test*123456789;Host=172.20.234.254;Port=5432;Database=curso_efcore_basico;")
    .Options;

var choice = 0;
do
{
    Console.WriteLine("Escolhe uma opção:");
    Console.WriteLine("1 - Adicionar Consumidor\n2 - Adicionar Livro\n3 - Recuperar Consumidor pelo Id\n4 - Recuperar Consumidor por Nome\n5 - Paginar Consumidores");
    Console.WriteLine("6 - Recuperar Livro pelo Id\n7 - Adicionar Loan\n8 - Ver os Livros de Um Consumidor\n9 - Ver os Consumidores e Um Livro\n10 - Detalhar Empréstimo");
    Console.WriteLine("0 - Sair");
    var tempChoice = Console.ReadLine()  ?? throw new InvalidOperationException("Can't be null");
    choice = int.Parse(tempChoice);

    switch (choice)
    {
        case 1:
            await AddCustomer.Execute(contextOptions);
            break;
        case 2:
            await AddBook.Execute(contextOptions);
            break;
        case 3:
            await GetCustomerById.Execute(contextOptions);
            break;
        case 4:
            await GetCustomerByName.Execute(contextOptions);
            break;
        case 5:
            await ListCustomerAsync.Execute(contextOptions);
            break;
        case 6:
            await GetBookById.Execute(contextOptions);
            break;
        case 7:
            await AddLoan.Execute(contextOptions);
            break;
        case 8:
            await GetBooksByCustomer.Execute(contextOptions);
            break;
        case 9:
            await GetCustomersByBook.Execute(contextOptions);
            break;
        case 10:
            await GetLoanDetails.Execute(contextOptions);
            break;
    }
}while(choice > 0);

Console.WriteLine("Saindo...");
namespace LoanSystem.Models;

public class CustomersByBook
{
    public string BookName { get; set; } = string.Empty;
    public string[] Customers { get; set; } = Array.Empty<string>();
}
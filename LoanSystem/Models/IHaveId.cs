namespace LoanSystem.Models;

public interface IHaveId
{
    int Id { get; }
}
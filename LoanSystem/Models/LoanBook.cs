namespace LoanSystem.Models;

public class LoanBook
{
    public int LoanId { get; set; }
    public Loan? Loan { get; set; }
    public int BookId { get; set; }
    public Book? Book { get; set; }
}
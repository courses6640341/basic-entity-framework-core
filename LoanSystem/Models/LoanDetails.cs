namespace LoanSystem.Models;

public class LoanDetails
{
    public string CustomerName { get; set; } = string.Empty;
    public string[] Books { get; set; } = Array.Empty<string>();
}
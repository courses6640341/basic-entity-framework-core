namespace LoanSystem.Models;

public class BooksByCustomer
{
    public string CustomerName { get; set; } = string.Empty;
    public string[] Books { get; set; } = Array.Empty<string>();
}
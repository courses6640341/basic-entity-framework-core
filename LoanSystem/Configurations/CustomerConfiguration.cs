using LoanSystem.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoanSystem.Configurations;

public class CustomerConfiguration: IEntityTypeConfiguration<Customer>
{
    public void Configure(EntityTypeBuilder<Customer> builder)
    {
        builder.HasOne<Address>(x => x.Address)
            .WithOne(x => x.Customer);

        builder.HasMany<Loan>(x => x.Loans)
            .WithOne(x => x.Customer);
    }
}
using LoanSystem.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoanSystem.Configurations;

public class LoanBookConfiguration : IEntityTypeConfiguration<LoanBook>
{
    public void Configure(EntityTypeBuilder<LoanBook> builder)
    {
        builder.HasKey(x => new { x.BookId, x.LoanId });

        builder.HasOne<Book>(x => x.Book)
            .WithMany(x => x.LoanBooks)
            .HasForeignKey(x => x.BookId);

        builder.HasOne<Loan>(x => x.Loan)
            .WithMany(x => x.LoanBooks)
            .HasForeignKey(x => x.LoanId);

        builder.HasIndex(x => x.BookId);
    }
}
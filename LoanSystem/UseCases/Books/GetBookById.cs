using LoanSystem.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.UseCases.Books;

public static class GetBookById
{
    public static async Task Execute(DbContextOptions<CustomDbContext> options)
    {
        await using var context = new CustomDbContext(options);
        var repository = new BookRepository(context);
        
        Console.WriteLine("Escreva o Id do livro:");
        var tempId = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");
        var id = int.Parse(tempId);

        var book = await repository.GetByIdAsync(id);
        
        Console.WriteLine($"Title : {book.Title}");
    }
}
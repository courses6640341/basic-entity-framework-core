using LoanSystem.Models;
using LoanSystem.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.UseCases.Books;

public static class AddBook
{
    public static async Task Execute(DbContextOptions<CustomDbContext> options)
    {
        Console.WriteLine("Digite o título do livro");
        var title = Console.ReadLine()  ?? throw new InvalidOperationException("Can't be null");
        
        await using var context = new CustomDbContext(options);
        var repository = new BookRepository(context);

        var bookToBeCreated = new Book
        {
            Title = title,
            IsAvailable = true
        };
    
        repository.Add(bookToBeCreated);
        await context.SaveChangesAsync();
    }
}
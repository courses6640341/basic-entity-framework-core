using LoanSystem.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.UseCases.Loan;

public static class GetLoanDetails
{
    public static async Task Execute(DbContextOptions<CustomDbContext> options)
    {
        await using var context = new CustomDbContext(options);
        var repository = new LoanRepository(context);
        
        Console.WriteLine("Escreva o Id do empréstimo:");
        var tempId = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");
        var id = int.Parse(tempId);

        var loanDetails = await repository.GetDetailedLoanBook(id);

        if (loanDetails is null)
        {
            Console.WriteLine("Empréstimo não encontrado");
            return;
        }
        
        Console.WriteLine($"Nome do Consumidor: {loanDetails.CustomerName}");
        foreach (var book in loanDetails.Books)
        {
            Console.WriteLine($"Livros: {book}");
        }
        Console.WriteLine();
    }
}
using LoanSystem.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.UseCases.Loan;

public static class GetCustomersByBook
{
    public static async Task Execute(DbContextOptions<CustomDbContext> options)
    {
        await using var context = new CustomDbContext(options);
        var repository = new LoanRepository(context);
        
        Console.WriteLine("Escreva o Id do livro:");
        var tempId = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");
        var id = int.Parse(tempId);

        var customersByBook = await repository.GetCustomersByBook(id);

        if (string.IsNullOrWhiteSpace(customersByBook.BookName) || !customersByBook.Customers.Any())
        {
            Console.WriteLine("Livro não encontrado");
            return;
        }
        
        Console.WriteLine($"Título do Livro: {customersByBook.BookName}");
        foreach (var customer in customersByBook.Customers)
        {
            Console.WriteLine($"Consumidor: {customer}");
        }
    }
}
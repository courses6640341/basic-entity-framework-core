using LoanSystem.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.UseCases.Loan;

public static class GetBooksByCustomer
{
    public static async Task Execute(DbContextOptions<CustomDbContext> options)
    {
        await using var context = new CustomDbContext(options);
        var repository = new LoanRepository(context);

        Console.WriteLine("Escreva o Id do consumidor:");
        var tempId = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");
        var id = int.Parse(tempId);

        var booksByCustomer = await repository.GetBooksByCustomer(id);

        if (booksByCustomer is null)
        {
            Console.WriteLine("Consumidor não encontrado");
            return;
        }

        Console.WriteLine($"Nome do Consumidor: {booksByCustomer.CustomerName}");
        foreach (var book in booksByCustomer.Books)
        {
            Console.WriteLine($"Livros: {book}");
        }

        Console.WriteLine();
    }
}
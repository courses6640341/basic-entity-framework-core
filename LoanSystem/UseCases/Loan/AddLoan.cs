using LoanSystem.Models;
using LoanSystem.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.UseCases.Loan;

public static class AddLoan
{
    public static async Task Execute(DbContextOptions<CustomDbContext> options)
    {
        await using var context = new CustomDbContext(options);
        var repository = new LoanRepository(context);
        
        Console.WriteLine("Digite o Id do consumidor:");
        var tempCustomerId = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");
        var customerId = int.Parse(tempCustomerId);

        var loan = new Models.Loan
        {
            Fee = .2m,
            StartDate = DateTime.UtcNow,
            ReturnDate = DateTime.UtcNow.AddDays(7)
        };

        int bookId;
        var booksIds = new List<int>();
        do
        {
            Console.WriteLine("Digite o Id do livro ou 0 para sair:");
            var tempBookId = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");
            bookId = int.Parse(tempBookId);
            if (bookId > 0)
            {
                booksIds.Add(bookId);
            }
        } while (bookId > 0);
        
        await repository.Add(loan, customerId, booksIds);
        await context.SaveChangesAsync();
    }
}
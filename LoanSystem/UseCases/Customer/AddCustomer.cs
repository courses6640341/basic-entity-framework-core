using LoanSystem.Models;
using LoanSystem.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.UseCases.Customer;

public static class AddCustomer
{
    public static async Task Execute(DbContextOptions<CustomDbContext> options)
    {
        Console.WriteLine("Digite o primeiro nome:");
        var firstName = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");
        
        Console.WriteLine("Digite o último nome:");
        var lastName = Console.ReadLine()  ?? throw new InvalidOperationException("Can't be null");
        
        Console.WriteLine("Digite o nome da rua:");
        var street = Console.ReadLine()  ?? throw new InvalidOperationException("Can't be null");;

        await using var context = new CustomDbContext(options);
        var repository = new CustomerRepository(context);

        var customerToBeCreated = new Models.Customer
        {
            FirstName = firstName,
            LastName = lastName,
            
            Address = new Address
            {
                Type = AddressType.Street,
                FullAddress = street
            }
        };

        repository.Add(customerToBeCreated);
        await context.SaveChangesAsync();
    }
}
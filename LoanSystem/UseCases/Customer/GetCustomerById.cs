using LoanSystem.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.UseCases.Customer;

public static class GetCustomerById
{
    public static async Task Execute(DbContextOptions<CustomDbContext> options)
    {
        await using var context = new CustomDbContext(options);
        var repository = new CustomerRepository(context);
        
        Console.WriteLine("Escreva o Id do consumidor:");
        var tempId = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");
        var id = int.Parse(tempId);

        var customer = await repository.GetByIdAsync(id);
        
        Console.WriteLine($"First Name: {customer.FirstName}");
        Console.WriteLine($"Last Name: {customer.LastName}");
        Console.WriteLine($"Street Name: {customer.Address!.FullAddress}");
    }
}
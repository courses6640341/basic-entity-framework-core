using LoanSystem.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.UseCases.Customer;

public static class GetCustomerByName
{
    public static async Task Execute(DbContextOptions<CustomDbContext> options)
    {
        await using var context = new CustomDbContext(options);
        var repository = new CustomerRepository(context);
        
        Console.WriteLine("Escreva o nome a procurar:");
        var name = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");

        var customers = await repository.GetByNameAsync(name);
        
        Console.WriteLine($"Count: {customers.Count}");
    }
}
using LoanSystem.Repositories;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.UseCases.Customer;

public static class ListCustomerAsync
{
    public static async Task Execute(DbContextOptions<CustomDbContext> options)
    {
        await using var context = new CustomDbContext(options);
        var repository = new CustomerRepository(context);
        
        Console.WriteLine("Escreva o número da página:");
        var tempPage = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");
        Console.WriteLine("Escreva o tamanho da página:");
        var tempPageSize = Console.ReadLine() ?? throw new InvalidOperationException("Can't be null");

        var page = int.Parse(tempPage);
        var pageSize = int.Parse(tempPageSize);
        
        var customers = await repository.ListAsync(page, pageSize);
        
        Console.WriteLine($"Count: {customers.Count}");
    }
}
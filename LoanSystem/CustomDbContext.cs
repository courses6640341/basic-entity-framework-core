using LoanSystem.Configurations;
using LoanSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem;

public class CustomDbContext: DbContext, IDatabaseContext
{
    private DbSet<Loan>? Loans  { get; set; }
    private DbSet<Customer>? Customers { get; set; }
    private DbSet<Book>? Books { get; set; }
    private DbSet<Address>? Addresses { get; set; }
    private DbSet<LoanBook>? LoanBooks { get; set; }

    public CustomDbContext()
    {
        
    }

    public CustomDbContext(DbContextOptions<CustomDbContext> options): base(options)
    {
        
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (optionsBuilder.IsConfigured) return;

        optionsBuilder
            .UseNpgsql("User ID=postuser;Password=Test*123456789;Host=172.20.234.254;Port=5432;Database=curso_efcore_basico;");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
    }
}
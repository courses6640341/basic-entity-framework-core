using LoanSystem.Models;
using LoanSystem.Repositories.Interfaces;

namespace LoanSystem.Repositories;

public class BookRepository: RepositoryBase<Book>, IBookRepository
{
    public BookRepository(CustomDbContext context) : base(context)
    {
    }
}
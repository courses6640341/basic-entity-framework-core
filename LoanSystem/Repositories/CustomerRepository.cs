using LoanSystem.Models;
using LoanSystem.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.Repositories;

public class CustomerRepository: ICustomerRepository
{
    private CustomDbContext _context;

    public CustomerRepository(CustomDbContext context)
    {
        _context = context;
    }

    public void Add(Customer customer)
    {
        _context.Add(customer);
    }

    public Task<Customer> GetByIdAsync(int id)
    {
       return  _context.Set<Customer>()
           .AsNoTracking()
            .Where(x => x.Id == id)
           .Include(x => x.Address)
            .FirstAsync();
    }

    public Task<List<Customer>> GetByNameAsync(string name)
    {
        return _context.Set<Customer>()
            .AsNoTracking()
            .Where(x => x.FirstName.Contains(name) || x.LastName.Contains(name))
            .ToListAsync();

    }

    public async Task<List<Customer>> ListAsync(int page, int pageSize)
    {

        if (page <= 0 || pageSize <= 0) throw new InvalidOperationException("Page and PageSize can't be zero or lower");
        
        var customersSet =  _context.Set<Customer>()
            .AsNoTracking()
            .OrderBy(x => x.Id);

        var itemsToSkip = (page - 1) * pageSize;
        var customerCount = await customersSet.CountAsync();
        if (itemsToSkip >= customerCount) throw new InvalidOperationException("Trying to access a non-existing page");

            var customers = await customersSet
                .Skip(itemsToSkip)
                .Take(pageSize)
                .ToListAsync();
            
        return customers;
    }
}
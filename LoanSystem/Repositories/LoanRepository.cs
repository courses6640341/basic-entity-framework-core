using LoanSystem.Models;
using LoanSystem.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace LoanSystem.Repositories;

public class LoanRepository: ILoanRepository
{
    private readonly CustomDbContext _context;

    public LoanRepository(CustomDbContext context)
    {
        _context = context;
    }

    public async Task Add(Loan loan, int customerId, IEnumerable<int> booksIds)
    {
        loan.CustomerId = customerId;
        foreach (var book in booksIds)
        {
            var loanBook = new LoanBook
            {
                Loan = loan,
                BookId = book
            };

            _context.Add(loanBook);
        }

        await _context.SaveChangesAsync();

    }

    public async Task<CustomersByBook> GetCustomersByBook(int bookId)
    {
        var loanBooks = await _context.Set<LoanBook>()
            .AsNoTracking()
            .Where(x => x.BookId == bookId)
            .Include(x => x.Book)
            .Include(x => x.Loan)
            .ThenInclude(x => x.Customer)
            .ToListAsync();

        if (!loanBooks.Any()) return new CustomersByBook();
        var firstElement = loanBooks.First();
        if (firstElement.Book is null) throw new InvalidOperationException("The book is null");

        var bookTitle = firstElement.Book.Title;
        var customers = loanBooks
            .Select(x => x.Loan!.Customer!.GetFullName())
            .ToArray();

        return new CustomersByBook
        {
            Customers = customers,
            BookName = bookTitle
        };
    }

    public async Task<BooksByCustomer?> GetBooksByCustomer(int customerId)
    {
        var loanBooks = await _context.Set<LoanBook>()
            .AsNoTracking()
            .Include(x => x.Loan)
            .ThenInclude(x => x.Customer)
            //Sim, caro aluno, podemos fazer where com propriedades de  navegação da mesma forma que fazemos where em outras tabelas do JOIN :-)
            .Where(x => x.Loan!.CustomerId == customerId) 
            .Include(x => x.Book)
            .ToListAsync();
        
        if (!loanBooks.Any()) return null;

        var firstElement = loanBooks.First();
        var customerName = firstElement.Loan!.Customer!.GetFullName();
        var books = loanBooks
            .Select(x => x.Book!.Title)
            .ToArray();
        
        return new BooksByCustomer()
        {
            Books = books,
            CustomerName = customerName
        };
    }

    public async Task<LoanDetails?> GetDetailedLoanBook(int loanId)
    {
        var loanBooks = await _context.Set<LoanBook>()
            .AsNoTracking()
            .Where(x => x.LoanId == loanId)
            .Include(x => x.Loan)
            .ThenInclude(x => x.Customer)
            .Include(x => x.Book)
            .ToListAsync();

       if (!loanBooks.Any()) return null;

       var firstElement = loanBooks.First();
       var customerName = firstElement.Loan!.Customer!.GetFullName();
       var books = loanBooks
           .Select(x => x.Book!.Title)
           .ToArray();

       return new LoanDetails
       {
           Books = books,
           CustomerName = customerName
       };

    }
}
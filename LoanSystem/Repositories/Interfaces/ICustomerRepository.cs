using LoanSystem.Models;

namespace LoanSystem.Repositories.Interfaces;

public interface ICustomerRepository
{
    void Add(Customer customer);
    Task<Customer> GetByIdAsync(int id);
    Task<List<Customer>> GetByNameAsync(string name);
    Task<List<Customer>> ListAsync(int page, int pageSize);
}
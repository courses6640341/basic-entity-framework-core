using LoanSystem.Models;

namespace LoanSystem.Repositories.Interfaces;

public interface IRepositoryBase<TEntity> where TEntity: class, IHaveId
{
    void Add(TEntity entity);
    Task<TEntity> GetByIdAsync(int id);
}
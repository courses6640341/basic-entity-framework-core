using LoanSystem.Models;

namespace LoanSystem.Repositories.Interfaces;

public interface ILoanRepository
{
    Task Add(Loan loan, int customerId, IEnumerable<int> booksIds);
    Task<CustomersByBook> GetCustomersByBook(int bookId);
    Task<BooksByCustomer?> GetBooksByCustomer(int customerId);
    Task<LoanDetails?> GetDetailedLoanBook(int loanId);
}
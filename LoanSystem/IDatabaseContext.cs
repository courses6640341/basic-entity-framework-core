namespace LoanSystem;

public interface IDatabaseContext
{
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}